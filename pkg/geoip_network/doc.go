// Package geoip_network provides the official client to [GeoIp.Network](https://www.geoip.network)
//
// This package uses the [GeoIP.Network API](https://api.geoip.network) to resolve IP addresses to locations.
// The API is free to use for up to 10000 requests per day, which can be upgraded to unlimited requests per day if you sponsor the project.
//
// The Library provides LookupIP, LookupCIDR, and LookupBulk as well as a Client type that provides the ability to supply your API credentials.
// The Library works with both IPv4 and IPv6 Addresses as well as ranges of addresses (CIDRs).
package geoip_network
